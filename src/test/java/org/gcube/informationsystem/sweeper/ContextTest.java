/**
 * 
 */
package org.gcube.informationsystem.sweeper;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.gcube.common.authorization.client.Constants;
import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.authorization.library.AuthorizationEntry;
import org.gcube.common.authorization.library.provider.AuthorizationProvider;
import org.gcube.common.authorization.library.provider.ClientInfo;
import org.gcube.common.authorization.library.provider.SecurityTokenProvider;
import org.gcube.common.authorization.library.utils.Caller;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author Luca Frosini (ISTI - CNR)
 *
 */
public class ContextTest {
	
	private static final Logger logger = LoggerFactory.getLogger(ContextTest.class);
	
	protected static Properties properties;
	protected static final String PROPERTIES_FILENAME = "token.properties";
	
	public static final String ROOT_TEST_CONTEXT;
	public static final String VO_DEFAULT_CONTEXT;
	public static final String VRE_DEFAULT_CONTEXT;
	public static final String VO_ALTERNATIVE_CONTEXT;
	public static final String VRE_ALTERNATIVE_CONTEXT;
	
	static {
		properties = new Properties();
		InputStream input = ContextTest.class.getClassLoader().getResourceAsStream(PROPERTIES_FILENAME);
		
		try {
			// load the properties file
			properties.load(input);
		} catch(IOException e) {
			throw new RuntimeException(e);
		}
		
		// PARENT_DEFAULT_TEST_SCOPE = "/pred4s"
		// DEFAULT_TEST_SCOPE_NAME = PARENT_DEFAULT_TEST_SCOPE + "/preprod";
		// ALTERNATIVE_TEST_SCOPE = DEFAULT_TEST_SCOPE_NAME + "/preVRE";
		
		
		ROOT_TEST_CONTEXT = "/gcube";
		VO_DEFAULT_CONTEXT = ROOT_TEST_CONTEXT + "/devNext";
		VRE_DEFAULT_CONTEXT = VO_DEFAULT_CONTEXT + "/NextNext";

		VO_ALTERNATIVE_CONTEXT = ROOT_TEST_CONTEXT + "/devsec";
		VRE_ALTERNATIVE_CONTEXT = VO_ALTERNATIVE_CONTEXT + "/devVRE";
		
		try {
			setContextByName(VRE_DEFAULT_CONTEXT);
		} catch(Exception e) {
			throw new RuntimeException(e);
		}

		
	}
	
	public static String getCurrentScope(String token) throws ObjectNotFound, Exception {
		AuthorizationEntry authorizationEntry = Constants.authorizationService().get(token);
		String context = authorizationEntry.getContext();
		logger.info("Context of token {} is {}", token, context);
		return context;
	}
	
	public static void setContextByName(String fullContextName) throws ObjectNotFound, Exception {
		String token = ContextTest.properties.getProperty(fullContextName);
		setContext(token);
	}
	
	private static void setContext(String token) throws ObjectNotFound, Exception {
		SecurityTokenProvider.instance.set(token);
		AuthorizationEntry authorizationEntry = Constants.authorizationService().get(token);
		ClientInfo clientInfo = authorizationEntry.getClientInfo();
		logger.debug("User : {} - Type : {}", clientInfo.getId(), clientInfo.getType().name());
		String qualifier = authorizationEntry.getQualifier();
		Caller caller = new Caller(clientInfo, qualifier);
		AuthorizationProvider.instance.set(caller);
		ScopeProvider.instance.set(getCurrentScope(token));
	}
	
	@BeforeClass
	public static void beforeClass() throws Exception {
		setContextByName(VO_DEFAULT_CONTEXT);
	}
	
	@AfterClass
	public static void afterClass() throws Exception {
		SecurityTokenProvider.instance.reset();
		ScopeProvider.instance.reset();
	}
	
}
