package org.gcube.informationsystem.sweeper;

import java.util.HashMap;
import java.util.Map;

import org.gcube.common.authorization.client.exceptions.ObjectNotFound;
import org.gcube.common.scope.api.ScopeProvider;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ISSweeperPluginTest {
	
	private static Logger logger = LoggerFactory.getLogger(ISSweeperPluginTest.class);
	
	@Test
	public void testLaunch() throws ObjectNotFound, Exception{
		String[] contexts = {
				ContextTest.VRE_ALTERNATIVE_CONTEXT,
				ContextTest.VRE_DEFAULT_CONTEXT,
				ContextTest.VO_ALTERNATIVE_CONTEXT,
				ContextTest.VO_DEFAULT_CONTEXT,
				ContextTest.ROOT_TEST_CONTEXT
		};
		
		for(String context : contexts){
			logger.info("\n\n\n-------------------------------------------------------------------------");
			ContextTest.setContextByName(context);
			try{
				Map<String, Object> inputs = new HashMap<String, Object>();
				inputs.put(ISSweeperPlugin.EXPIRING_MINUTES_TIMEOUT, 30);
				inputs.put(ISSweeperPlugin.DEAD_DAYS_TIMEOUT, 1);
				logger.debug("Inputs : {}", inputs);
				ISSweeperPlugin isExporterPlugin = new ISSweeperPlugin(new ISSweeperPluginDeclaration());
				isExporterPlugin.launch(inputs);
			}catch (Exception e) {
				logger.error("Sweeper error on {}", ScopeProvider.instance.get(), e);
			}
			logger.info("\n\n\n");
		}
	}

}
